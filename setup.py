import setuptools

setuptools.setup(
    name='aws_cost_optimization',
    version='0.0.2',
    packages=setuptools.find_packages(),
    url='',
    license='',
    author='Dheeraj Banodha',
    author_email='dheerajmbanodha@gmail.com',
    description=''
)