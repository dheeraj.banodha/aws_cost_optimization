# Release the EIP with allocation_id
def release_eip(self, allocation_id: str) -> dict:
    client = self.session.client('ec2')

    response = client.release_address(
        AllocationId=allocation_id,  # for ec2-VPC
        # PublicIp=input["public_ip"],              #for ec2-classic
    )
    return response


# tag resources of ResourceARN with Tags
def tag_resource(self, resource_arn_list: list, tags: dict) -> dict:
    client = self.session.client('resourcegroupstaggingapi')

    response = client.tag_resources(
        ResourceARNList=resource_arn_list,
        Tags=tags
    )
    return response


# Stop the EC2 instance
def stop_ec2_instance(self, instances: list) -> dict:
    client = self.session.client('ec2')

    response = client.stop_instances(InstanceIds=instances)

    return response


# Stop the RDS Instance
def stop_rds_instance(self, db_instance: str) -> dict:
    client = self.session.client('rds')
    response = client.stop_db_instance(
        DBInstanceIdentifier=db_instance,
        # DBSnapshotIdentifier='string'
    )
    return response


# put logs retention policy on log groups
def put_retention_policy(self, log_group_name: str, retention_in_days: int) -> dict:
    client = self.session.client('logs')
    response = client.put_retention_policy(
        logGroupName=log_group_name,
        retentionInDays=retention_in_days
    )

    return response


# modify the volume
def modify_volume(self, volume_id: str, volume_type: str) -> dict:
    client = self.session.client('ec2')
    response = client.modify_volume(
        VolumeId=volume_id,
        VolumeType=volume_type,
    )

    return response
