from aws_cost_optimization import actions as act
from boto3 import session


class aws_client:
    def __init__(self, aws_access_key_id, aws_secret_access_key, region_name):
        # self.aws_access_key_id = aws_access_key_id
        # self.aws_secret_access_key = aws_secret_access_key
        # self.region_name = region_name
        self.session = session.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name
        )

    tag_resource = act.tag_resource
    release_eip = act.release_eip
    stop_ec2_instance = act.stop_ec2_instance
    stop_rds_instance = act.stop_rds_instance
    put_retention_policy = act.put_retention_policy
    modify_volume = act.modify_volume
